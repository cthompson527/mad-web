import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import * as React from 'react';
import Routes from './components/Routes';

const muiTheme = createMuiTheme({
  palette: {
    common: { black: '#000', white: '#fff' },
    background: { paper: '#fff', default: '#fafafa' },
    primary: {
      light: 'rgba(110, 255, 255, 1)',
      main: 'rgba(0, 229, 255, 1)',
      dark: 'rgba(0, 178, 204, 1)',
      contrastText: 'rgba(0, 0, 0, 1)',
    },
    secondary: {
      light: 'rgba(255, 221, 113, 1)',
      main: 'rgba(255, 171, 64, 1)',
      dark: 'rgba(199, 124, 2, 1)',
      contrastText: 'rgba(0, 0, 0, 1)',
    },
    error: {
      light: '#e57373',
      main: '#f44336',
      dark: '#d32f2f',
      contrastText: '#fff',
    },
    text: {
      primary: 'rgba(0, 0, 0, 0.87)',
      secondary: 'rgba(0, 0, 0, 0.54)',
      disabled: 'rgba(0, 0, 0, 0.38)',
      hint: 'rgba(0, 0, 0, 0.38)',
    },
  },
  typography: {
    useNextVariants: true,
  },
});

const App = () => (
  <MuiThemeProvider theme={muiTheme}>
    <Routes />
  </MuiThemeProvider>
);

export default App;

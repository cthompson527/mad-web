import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ExitIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';
import * as React from 'react';
import { Link } from 'react-router-dom';

import bracketLogo from '../icons/Brackets.svg';
import trophyLogo from '../icons/Trophy.svg';

const Brackets = () => (
  <img src={bracketLogo} alt="My Brackets" height="24px" width="24px" />
);
const Trophy = () => (
  <img src={trophyLogo} alt="Trophy" height="24px" width="24px" />
);

interface ISideBarProps {
  children: React.ReactElement<{}>;
  open: boolean;
  text: string;
}

class SideBarItem extends React.PureComponent<ISideBarProps, {}> {
  render() {
    const { children, open, text } = this.props;

    const withToolTip = (
      <Tooltip title={text} placement="right">
        {children}
      </Tooltip>
    );

    if (open) {
      return children;
    } else {
      return withToolTip;
    }
  }
}

export default (props: { open: boolean }) => (
  <div>
    <List>
      <Link to="/">
        <SideBarItem open={props.open} text="Dashboard">
          <ListItem button>
            <ListItemIcon>
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
        </SideBarItem>
      </Link>
      <Link to="/leagues">
        <SideBarItem open={props.open} text="Leagues">
          <ListItem button>
            <ListItemIcon>
              <Trophy />
            </ListItemIcon>
            <ListItemText primary="Trophy" />
          </ListItem>
        </SideBarItem>
      </Link>
      <SideBarItem open={props.open} text="My Brackets">
        <ListItem button>
          <ListItemIcon>
            <Brackets />
          </ListItemIcon>
          <ListItemText primary="My Brackets" />
        </ListItem>
      </SideBarItem>
      <SideBarItem open={props.open} text="Settings">
        <ListItem button>
          <ListItemIcon>
            <SettingsIcon />
          </ListItemIcon>
          <ListItemText primary="Settings" />
        </ListItem>
      </SideBarItem>
      <SideBarItem open={props.open} text="Logoff">
        <ListItem button>
          <ListItemIcon>
            <ExitIcon />
          </ListItemIcon>
          <ListItemText primary="Logoff" />
        </ListItem>
      </SideBarItem>
    </List>
  </div>
);

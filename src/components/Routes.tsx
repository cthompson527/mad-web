import * as React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Bracket from '../pages/Bracket';
import Index from '../pages/Index';
import Leagues from '../pages/Leagues';
import Navigation from './Navigation';

const Routes = () => (
  <React.Fragment>
    <Router>
      <div>
        <Route
          path="/"
          render={props => (
            <Navigation {...props}>
              <Route
                exact
                path="/"
                render={routeProps => <Index {...routeProps} />}
              />
              <Route
                exact
                path="/leagues"
                render={routeProps => <Leagues {...routeProps} />}
              />
              <Route
                exact
                path="/bracket/:name"
                render={routeProps => <Bracket {...routeProps} />}
              />
            </Navigation>
          )}
        />
      </div>
    </Router>
  </React.Fragment>
);

export default Routes;

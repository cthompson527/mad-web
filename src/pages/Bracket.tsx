import * as React from 'react';
import { match } from 'react-router';
import fakeData from '../util/fakeData';
import './Bracket.css';

interface IGame {
  id: number;
  round: number;
  home: string;
  away: string;
  seeds: number;
  seedsTeam: string;
}

type OnUpdate = (
  id: number,
  bracket: string,
  homeAway: string,
  selection: string,
) => void;

interface IBracketProps {
  reverse: boolean;
  title: string;
  games: IGame[];
  pickedGames: string[];
  onUpdate: OnUpdate;
}

const displayGames = (props: IBracketProps, round: number) => (
  <div>
    {props.games
      .filter((game: IGame) => game.round === round)
      .map((game: IGame) => (
        <ul className="matchup" key={game.id}>
          <li
            className={`team team-top ${
              game.home === props.pickedGames[game.id] ? 'correct' : ''
            }`}
            onClick={() =>
              props.onUpdate(
                game.id,
                props.title.toLowerCase(),
                game.seedsTeam,
                game.home,
              )
            }
          >
            {game.home}
          </li>
          <li
            className={`team team-bottom ${
              game.away === props.pickedGames[game.id] ? 'correct' : ''
            }`}
            onClick={() =>
              props.onUpdate(
                game.id,
                props.title.toLowerCase(),
                game.seedsTeam,
                game.away,
              )
            }
          >
            {game.away}
          </li>
        </ul>
      ))}
  </div>
);

const Bracket = (props: IBracketProps) => (
  <div className="split split-one">
    <div className="bracket-name round-details">
      <h1>{props.title}</h1>
    </div>
    <div className={`games ${props.reverse ? 'reverse' : ''}`}>
      <div className="round round-one current">
        <div className="round-details">
          Round 64
          <br />
          <span className="date">March 16</span>
        </div>
        {displayGames(props, 64)}
      </div>

      <div className="round round-two current">
        <div className="round-details">
          Round 32
          <br />
          <span className="date">March 18</span>
        </div>
        {displayGames(props, 32)}
      </div>

      <div className="round round-three current">
        <div className="round-details">
          Round 16
          <br />
          <span className="date">March 22</span>
        </div>
        {displayGames(props, 16)}
      </div>

      <div className="round round-four current">
        <div className="round-details">
          Round 8<br />
          <span className="date">March 26</span>
        </div>
        {displayGames(props, 8)}
      </div>
    </div>
  </div>
);

const Champion = (props: {
  finalFour: IGame[];
  championship: IGame[];
  winner: string;
  pickedGames: string[];
  onUpdate: OnUpdate;
}) => {
  const leftFinalFour = props.finalFour[0];
  const rightFinalFour = props.finalFour[1];
  const championship = props.championship[0];
  return (
    <div className="champion">
      <div className="final">
        <div className="round-details">
          Final Four
          <br />
          <span className="date">March 26-28</span>
        </div>
        <ul className="matchup current">
          <li
            className={`team team-top ${
              leftFinalFour.home === props.pickedGames[leftFinalFour.id]
                ? 'correct'
                : ''
            }`}
            onClick={() =>
              props.onUpdate(
                leftFinalFour.id,
                'finalFour',
                'home',
                leftFinalFour.home,
              )
            }
          >
            {leftFinalFour.home}
          </li>
          <li
            className={`team team-bottom ${
              leftFinalFour.away === props.pickedGames[leftFinalFour.id]
                ? 'correct'
                : ''
            }`}
            onClick={() =>
              props.onUpdate(
                leftFinalFour.id,
                'finalFour',
                'home',
                leftFinalFour.away,
              )
            }
          >
            {leftFinalFour.away}
          </li>
        </ul>
      </div>

      <div className="championship">
        <div>
          <div className="round-details">
            championship <br />
            <span className="date">March 30 - Apr. 1</span>
          </div>
          <ul className="matchup current">
            <li
              className={`team team-top ${
                championship.home === props.pickedGames[championship.id]
                  ? 'correct'
                  : ''
              }`}
              onClick={() =>
                props.onUpdate(
                  championship.id,
                  'championship',
                  'winner',
                  championship.home,
                )
              }
            >
              {championship.home}
            </li>
            <li
              className={`team team-bottom ${
                championship.away === props.pickedGames[championship.id]
                  ? 'correct'
                  : ''
              }`}
              onClick={() =>
                props.onUpdate(
                  championship.id,
                  'championship',
                  'winner',
                  championship.away,
                )
              }
            >
              {championship.away}
            </li>
          </ul>
        </div>
        <div>
          <div
            className="round-details"
            style={{ height: 0, marginTop: 20, marginBottom: 5 }}
          >
            winner
          </div>
          <ul className="matchup current">
            <li className="team winner">{props.winner}</li>
          </ul>
        </div>
      </div>

      <div className="final">
        <div className="round-details">
          Final Four
          <br />
          <span className="date">March 26-28</span>
        </div>
        <ul className="matchup current">
          <li
            className={`team team-top ${
              rightFinalFour.home === props.pickedGames[rightFinalFour.id]
                ? 'correct'
                : ''
            }`}
            onClick={() =>
              props.onUpdate(
                rightFinalFour.id,
                'finalFour',
                'away',
                rightFinalFour.home,
              )
            }
          >
            {rightFinalFour.home}
          </li>
          <li
            className={`team team-bottom ${
              rightFinalFour.away === props.pickedGames[rightFinalFour.id]
                ? 'correct'
                : ''
            }`}
            onClick={() =>
              props.onUpdate(
                rightFinalFour.id,
                'finalFour',
                'away',
                rightFinalFour.away,
              )
            }
          >
            {rightFinalFour.away}
          </li>
        </ul>
      </div>
    </div>
  );
};

interface IProps {
  match: match<{ name: string }>;
}

interface IState {
  games: typeof fakeData;
  pickedGames: string[];
}

// tslint:disable
export default class BracketComponent extends React.PureComponent<
  IProps,
  IState
> {
  constructor(props: IProps) {
    super(props);
    this.state = { games: fakeData, pickedGames: new Array(64) };
  }

  onSelection = (
    id: number,
    bracket: string,
    homeAway: string,
    selection: string,
  ) => {
    const games = { ...this.state.games };

    let bracketGames = games[bracket] as IGame[];
    const gameIndex = bracketGames.findIndex(g => g.id === id);
    if (
      bracketGames[gameIndex].home === '' ||
      bracketGames[gameIndex].away === ''
    ) {
      return;
    }
    const { seeds } = bracketGames[gameIndex];
    if (seeds === 61 || seeds === 62) {
      bracketGames = this.state.games.finalFour;
    } else if (seeds === 63) {
      bracketGames = this.state.games.championship;
    } else if (seeds === 64) {
      games.winner.team = selection;
      const pickedGames = this.state.pickedGames.slice();
      pickedGames[id] = selection;
      this.setState({ games, pickedGames });
      return;
    }
    const gameSeedsIndex = bracketGames.findIndex(g => seeds === g.id);
    bracketGames[gameSeedsIndex][homeAway] = selection;

    const pickedGames = this.state.pickedGames.slice();
    pickedGames[id] = selection;

    this.setState({ games, pickedGames });
  };

  render() {
    const { match } = this.props;
    const { games, pickedGames } = this.state;
    return (
      <div>
        <h1>{match.params.name}'s Bracket</h1>
        <div className="main">
          <section id="bracket">
            <div className="container">
              <Bracket
                title="South"
                reverse={false}
                games={games.south}
                pickedGames={pickedGames}
                onUpdate={this.onSelection}
              />
              <Bracket
                title="East"
                reverse={true}
                games={games.east}
                pickedGames={pickedGames}
                onUpdate={this.onSelection}
              />
            </div>

            <div className="container">
              <Champion
                finalFour={games.finalFour}
                championship={games.championship}
                winner={games.winner.team}
                pickedGames={pickedGames}
                onUpdate={this.onSelection}
              />
            </div>

            <div className="container">
              <Bracket
                title="West"
                reverse={false}
                games={games.west}
                pickedGames={pickedGames}
                onUpdate={this.onSelection}
              />
              <Bracket
                title="MidWest"
                reverse={true}
                games={games.midwest}
                pickedGames={pickedGames}
                onUpdate={this.onSelection}
              />
            </div>
          </section>
        </div>
      </div>
    );
  }
}

import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/AddCircle';
import classNames from 'classnames';
import { History } from 'history';
import * as React from 'react';

const fakeData = [
  {
    leagueName: '1st and 30',
    players: [
      {
        firstName: 'Christina',
        lastName: 'Thompson',
        points: 60,
      },
      {
        firstName: 'Matt',
        lastName: 'Espinoza',
        points: 75,
      },
      {
        firstName: 'Cory',
        lastName: 'Thompson',
        points: 50,
      },
    ],
  },
  {
    leagueName: '1st and 35',
    players: [
      {
        firstName: 'Christina',
        lastName: 'Thompson',
        points: 70,
      },
      {
        firstName: 'Matt',
        lastName: 'Espinoza',
        points: 60,
      },
      {
        firstName: 'Cory',
        lastName: 'Thompson',
        points: 75,
      },
      {
        firstName: 'Travis',
        lastName: 'Mandel',
        points: 50,
      },
    ],
  },
];

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    title: {
      justifyContent: 'space-between',
    },
    button: {
      margin: theme.spacing.unit,
      minWidth: 120,
    },
    addIcon: {
      marginLeft: 15,
    },
    formControl: {
      margin: theme.spacing.unit,
      minWidth: 200,
    },
    selectEmpty: {
      marginTop: theme.spacing.unit * 2,
    },
  });

interface IProps extends WithStyles<typeof styles> {
  history: History;
}

interface IState {
  league: string;
}

class Leagues extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      league: '',
    };
  }

  handleSelectChange = (
    event: React.ChangeEvent<HTMLSelectElement>,
    child: React.ReactElement<{ value: string }>,
  ) => {
    this.setState({
      league: child.props.value,
    });
  };

  render() {
    const { classes } = this.props;

    const CreateButton = (
      <Button variant="contained" color="primary" className={classes.button}>
        Create League <AddIcon className={classes.addIcon} />
      </Button>
    );

    const SelectButton = (
      <FormControl className={classes.formControl}>
        <Select
          value={this.state.league}
          onChange={this.handleSelectChange}
          name="league"
          displayEmpty
          className={classes.selectEmpty}
        >
          <MenuItem value="" disabled>
            League
          </MenuItem>
          {fakeData.map(data => (
            <MenuItem key={data.leagueName} value={data.leagueName}>
              {data.leagueName}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );

    const fakeLeague = fakeData.filter(
      league => league.leagueName === this.state.league,
    );

    const LeaguesTable = (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Current Points</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {fakeLeague.length > 0 &&
            fakeLeague[0].players
              .slice()
              .sort((first, second) => second.points - first.points)
              .map(player => (
                <TableRow
                  onClick={event => {
                    this.props.history.push(
                      `/bracket/${event.currentTarget.id}`,
                    );
                  }}
                  key={`${player.firstName}${player.lastName}`}
                  id={`${player.firstName}${player.lastName}`}
                >
                  <TableCell>
                    {`${player.firstName} ${player.lastName}`}
                  </TableCell>
                  <TableCell>{player.points}</TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>
    );

    return (
      <div className={classNames(classes.root, classes.title)}>
        {SelectButton}
        {CreateButton}
        {LeaguesTable}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Leagues);

import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import * as React from 'react';

const styles = (theme: Theme) => {
  return createStyles({
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
      overflow: 'auto',
    },
  });
};

interface IProps extends WithStyles<typeof styles> {}

const C = (props: IProps) => (
  <main className={props.classes.content}>
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>League Name</TableCell>
          <TableCell>League Leader</TableCell>
          <TableCell>Current Points</TableCell>
          <TableCell>Current Position</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell>1st and 30</TableCell>
          <TableCell>Matt</TableCell>
          <TableCell>50</TableCell>
          <TableCell>3rd</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>1st and 35</TableCell>
          <TableCell>Cory</TableCell>
          <TableCell>75</TableCell>
          <TableCell>1st</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </main>
);

export default withStyles(styles, { withTheme: true })(C);
